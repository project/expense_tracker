(function($, Drupal, drupalSettings) {
  Drupal.behaviors.testimonials = {
    attach: function(context, settings) {
      $(document, context).once('testimonials').each(function(element) {
        $('.et-reports').each(function() {
          var owlCarousel = $(this);
          if (owlCarousel.data('report-categories')) {
            var categories = owlCarousel.data('report-categories');
            if (owlCarousel.data('report-income')) {
              var income = owlCarousel.data('report-income');
              var expense = owlCarousel.data('report-expense');
              var report_type = 'Income and expense';
              var series = [{
                name: 'Expense',
                color: '#F75D59',
                data: income
              }, {
                name: 'Income',
                data: expense
              }];
            } else {
              var series_data = owlCarousel.data('report-series');
              var report_type = owlCarousel.data('report-type');
              var color = '';
              var gradiant_start = '#7cb5ec';
              if (report_type == 'expense') {
                color = '#F75D59';
                gradiant_start = '#f8001a2b';
              }
              var series = [{
                color: color,
                name: report_type,
                data: series_data
              }];
            }
            var reportsData = {
              title: {
                text: 'Financial Statement Analysis Chart'
              },
              subtitle: {
                text: report_type + ' statement'
              },
              chart: {
                renderTo: owlCarousel.attr('id'),
                marginBottom: 80
              },
              plotOptions: {
                series: {
                  marker: {
                    enabled: true,
                    radius: 2,
                    fillColor: '#FFFFFF',
                    lineWidth: 2,
                    lineColor: null
                  },
                  states: {
                    hover: {
                      halo: {
                        size: 0
                      }
                    }
                  }
                },
              },
              series: series,
              tooltip: {
                formatter: function() {
                  return 'The value for <b>' + this.x + '</b> is <b>$' + this.y + '</b>, in series ' + this.series.name;
                }
              },
              yAxis: [{
                title: {
                  text: 'Price'
                }
              }],
              xAxis: [{
                title: {
                  text: 'Time'
                },
                categories: categories,
                labels: {
                  rotation: 0
                }
              }],
            };
            var chart = new Highcharts.Chart(reportsData);
            if ($('#piechart').length > 0) {
              var total_income = $('#piechart').data('total-income-amount');
              var total_expense = $('#piechart').data('total-expense-amount');
              var piechart = new Highcharts.chart('piechart', {
                chart: {
                  type: 'pie'
                },
                title: {
                  text: 'Income vs Expense'
                },
                colors: ['#7CB5EC', '#F75D59'],
                series: [{
                  data: [{
                    name: 'Income',
                    y: total_income
                  }, {
                    name: 'Expense',
                    y: total_expense
                  }]
                }]
              });
            }
          }
          if ($('.highchart_container').length > 0) {
            $(function() {
              $('.highchart_container').each(function() {
                var highchart_container = $(this);
                var categories = highchart_container.data('report-categories');
                var series_res_data = highchart_container.data('report-series');
                var report_type = highchart_container.data('report-type');
                var series_data = [];
                var color = '#7CB5EC';
                if (report_type == 'expense') {
                  color = '#F75D59';
                }
                series_res_data.forEach(ele => series_data.push(+ele))
                highchart_container.highcharts({
                  chart: {
                    type: 'bar',
                    height: categories.length * 20 + 200
                  },
                  colors: [color],
                  title: {
                    text: 'Transaction reports',
                  },
                  subtitle: {
                    text: report_type + ' categories'
                  },
                  xAxis: {
                    categories: categories,
                    title: {
                      text: null
                    }
                  },
                  yAxis: {
                    min: 0,
                    title: {
                      text: report_type + ' categories',
                    },
                    labels: {}
                  },
                  tooltip: {
                    valueSuffix: ''
                  },
                  plotOptions: {
                    bar: {
                      dataLabels: {
                        enabled: true
                      }
                    },
                  },
                  credits: {
                    enabled: false
                  },
                  series: [{
                    data: series_data,
                    name: report_type + ' statements'
                  }],
                });
              });
            });
          }
          $(function() {
            Highcharts.setOptions({
              chart: {
                style: {
                  fontFamily: 'Arial, Helvetica, sans-serif',
                  fontSize: '2em',
                  color: '#f00'
                }
              }
            });
            $('#barchart11').highcharts({
              chart: {
                type: 'column'
              },
              colors: ['#ED5565', '#5D9CEC', '#A0D468', '#FFCE54', '#48CFAD', '#AC92EC', '#AAB2BD', '#D770AD', '#c42525', '#a6c96a'],
              title: {
                text: 'Example Chart',
                style: {
                  color: '#555'
                }
              },
              legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom',
                borderWidth: 0,
                backgroundColor: '#FFFFFF'
              },
              xAxis: {
                categories: ['2006', '2007', '2008', '2009', '2010', '2011']
              },
              yAxis: {
                title: {
                  text: ''
                }
              },
              tooltip: {
                shared: false,
                valueSuffix: 'points'
              },
              credits: {
                enabled: false
              },
              plotOptions: {
                areaspline: {
                  fillOpacity: 0.1
                },
                series: {
                  groupPadding: .15
                }
              },
              series: [{
                name: 'Data 1',
                data: [3, 4, 3, 5, 4, 9]
              }, {
                name: 'Data 2',
                data: [1, 3, 4, 3, 3, 5]
              }, {
                name: 'Data 3',
                data: [5, 6, 1, 2, 5, 7]
              }, {
                name: 'Data 4',
                data: [4, 2, 5, 6, 3, 4]
              }, {
                name: 'Data 5',
                data: [1, 3, 2, 5, 7, 3]
              }, {
                name: 'Data 6',
                data: [2.5, 1, 5.5, 8.6, 7.3, 2.4]
              }, {
                name: 'Data 7',
                data: [1, 3, 4, 6, 2, 3]
              }, {
                name: 'Data 8',
                data: [5, 3, 4, 3, 2, 5]
              }]
            });
          });
        });
if ($('#charthere').length > 0) {
  var categories = $('#charthere').data('report-categories');
  var series = $('#charthere').data('report-series');
  var report_type = $('#charthere').data('report-type');
  var series_data = [];
  for (let i = 0; i < categories.length; i++) {
    series_data[i] = {
      "data": [
      [categories[i], series[i]]
      ],
      "name": categories[i]
    };
  }
  console.log('categories', categories);
  console.log('series', series);
  console.log('series_data', series_data);
  var charthere = new Highcharts.chart('charthere', {
    chart: {
      type: 'column'
    },
    plotOptions: {
      column: {
        pointWidth: 80
      }
    },
    legend: {
      align: 'right',
      verticalAlign: 'top',
      x: 280,
      y: 45,
      floating: true,
      width: 555,
      itemWidth: 500,
      itemStyle: {
        width: 500
      }
    },
    title: {
      text: 'Financial Statement Analysis Chart'
    },
    subtitle: {
      text: '10 year ' + report_type + ' report'
    },
    xAxis: {
      title: {
        text: report_type
      },
      labels: {},
      type: "category"
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Totals'
      },
      labels: {
        format: '{value}'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0"></td>' + '<td style="padding:0"><b>{point.y}</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    series: series_data,
  });
}
});
}
}
}(jQuery, Drupal, drupalSettings));