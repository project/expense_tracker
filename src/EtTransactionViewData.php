<?php

namespace Drupal\expense_tracker;

use Drupal\views\EntityViewsData;

/**
 * Render controller for et_transactions.
 */
class EtTransactionViewData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['et_transaction_field_data']['transactions'] = array(
      'title' => 'Total transactions',
      'help' => 'Displays the total number of transactions.',
      'real field' => 'id',
      'field' => array(
        'id' => 'et_transaction_totaltransactions',
      ),
    );

    $data['et_transaction_field_data']['status_with_runtime'] = array(
      'title' => 'Active with runtime',
      'help' => 'Displays the status with runtime.',
      'real field' => 'id',
      'field' => array(
        'id' => 'et_transaction_status',
      ),
    );

    return $data;
  }

}
