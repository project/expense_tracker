<?php

namespace Drupal\expense_tracker\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting a et_transaction.
 */
class EtTransactionDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this transaction %transaction', array('%transaction' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return t('All associated transactions will be deleted too. This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->t('Are you sure you want to delete this et_transaction %et_transaction', array('%et_transaction' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('expense_tracker.et_transaction_list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    \Drupal::logger('et_transaction')->notice('EtTransaction %et_transaction deleted.', array('%et_transaction' => $this->entity->label()));
    $this->messenger()->addMessage($this->t('The et_transaction %et_transaction has been deleted.', array('%et_transaction' => $this->entity->label())));
    $form_state->setRedirect('expense_tracker.et_transaction_list');
  }

}
