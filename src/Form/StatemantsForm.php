<?php

namespace Drupal\expense_tracker\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;

/**
 * Class StatemantsForm.
 */
class StatemantsForm extends FormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * Constructs a new StatemantsForm object.
   */
  public function __construct(
    MessengerInterface $messenger
  ) {
    $this->messenger = $messenger;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ex82_hello_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $statement_options = array(
     'last_seven_days' => $this->t('Last 7 days'),
     'last_thirty_days' => $this->t('Last 30 days'),
     'this_week' => $this->t('This week'),
     'last_week' => $this->t('Last week'),
     'this_month' => $this->t('This month'),
     'last_month' => $this->t('Last month'),
     'this_quarter' => $this->t('This Quarter'),
     'last_quarter' => $this->t('Last Quarter'),
     'this_year' => $this->t('This year'),
     'last_year' => $this->t('Last year')
   );

    $form['transaction_type'] = array(
      '#type' => 'select',
      '#title' => t('Transaction type'),
      '#options' => $statement_options,
      '#description' => t('<small>Filter financial statements by transaction type</small>'),
      '#default_value' => '',
      '#options' => array(
        'All' => t('All'),
        '1' => t('Expense'),
        '2' => t('Income'),
      ),
    );

    $form['statement_type'] = array(
      '#type' => 'radios',
      '#title' => t('Analyze income and expense statement'),
      '#options' => $statement_options,
      '#description' => t('Filter financial statements by specific date range'),
      '#default_value' => 'last_seven_days',
    );

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.


    $form_state->setRedirect('user.page');

    $query = array();

    $statement_type = $form_state->getValue('statement_type');
    $transaction_type = $form_state->getValue('transaction_type');
    switch ($statement_type) {

      case 'last_seven_days':
      $yesterday = strtotime("-1 day");
      $last_seven_days_from = strtotime("-8 day");
      $lsd_start = date("Y-m-d",$last_seven_days_from);
      $lsd_end = date("Y-m-d",$yesterday);
      $query['from'] = $lsd_start;
      $query['to'] = $lsd_end;

      break;

      case 'last_thirty_days':
      $yesterday = strtotime("-1 day");
      $last_seven_days_from = strtotime("-30 day");
      $lsd_start = date("Y-m-d",$last_seven_days_from);
      $lsd_end = date("Y-m-d",$yesterday);
      $query['from'] = $lsd_start;
      $query['to'] = $lsd_end;

      break;

      case 'this_week':
      $lsd_start = date('Y-m-d', strtotime("this week"));
      $today = time();
      $lsd_end = date("Y-m-d",$today);
      $query['from'] = $lsd_start;
      $query['to'] = $lsd_end;

      break;

      case 'last_week':
      $lsd_start = date('Y-m-d', strtotime("this week -8 day"));
      $lsd_end = date('Y-m-d', strtotime("this week -2 day"));
      $query['from'] = $lsd_start;
      $query['to'] = $lsd_end;

      break;

      case 'this_month':
      $lsd_start = date('Y-m-d', strtotime("first day of this month"));
      $lsd_end = date('Y-m-d', strtotime("last day of this month"));
      $query['from'] = $lsd_start;
      $query['to'] = $lsd_end;

      break;

      case 'last_month':

      $lsd_start = date('Y-m-d', strtotime("first day of last month"));
      $lsd_end = date('Y-m-d', strtotime("last day of last month"));
      $query['from'] = $lsd_start;
      $query['to'] = $lsd_end;

      break;

      case 'this_quarter':

      $current_quarter = ceil(date('n') / 3);
      $lsd_start = date('Y-m-d', strtotime(date('Y') . '-' . (($current_quarter * 3) - 2) . '-1'));
      $lsd_end = date('Y-m-t', strtotime(date('Y') . '-' . (($current_quarter * 3)) . '-1'));

      $query['from'] = $lsd_start;
      $query['to'] = $lsd_end;
      break;

      case 'last_quarter':
      $last_quarter = ceil(date('n') / 3)-1;
      $lsd_start = date('Y-m-d', strtotime(date('Y') . '-' . (($last_quarter * 3) - 2) . '-1'));
      $lsd_end = date('Y-m-t', strtotime(date('Y') . '-' . (($last_quarter * 3)) . '-1'));

      $query['from'] = $lsd_start;
      $query['to'] = $lsd_end;

      break;

      case 'this_year':

      $lsd_start = date('Y-m-d', strtotime("first day of january this year"));
      $lsd_end = date('Y-m-d', strtotime("last day of december this year"));
      $query['from'] = $lsd_start;
      $query['to'] = $lsd_end;

      break;

    }

    $query['referrer'] = 'statements';
    $query['transaction_type'] = $transaction_type;

    $url = Url::fromRoute('expense_tracker.et_transaction_list', $query);
    $form_state->setRedirectUrl($url);


  }

}