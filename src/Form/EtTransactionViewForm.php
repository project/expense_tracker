<?php

namespace Drupal\expense_tracker\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\expense_tracker\EtTransactionInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays banned IP addresses.
 */
class EtTransactionViewForm extends FormBase implements BaseFormIdInterface {

  /**
   * The EtTransaction of the form.
   *
   * @var \Drupal\expense_tracker\EtTransactionInterface
   */
  protected $et_transaction;

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'et_transaction_view_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'et_transaction_view_form_' . $this->et_transaction->id();
  }

  /**
   * Set the EtTransaction of this form.
   *
   * @param \Drupal\expense_tracker\EtTransactionInterface $et_transaction
   *   The et_transaction that will be set in the form.
   */
  public function setEtTransaction(EtTransactionInterface $et_transaction) {
    $this->et_transaction = $et_transaction;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL, $view_mode = 'full') {
    // Add the et_transaction to the form.
    $form['et_transaction']['#type'] = 'value';
    $form['et_transaction']['#value'] = $this->et_transaction;

    $form['#view_mode'] = $view_mode;

    $form['actions'] = $this->actions($form, $form_state, $this->et_transaction);

    $form['#cache'] = array(
      'tags' => $this->et_transaction->getCacheTags(),
    );

    return $form;
  }

  /**
   * Ajax callback to replace the et_transaction form.
   */
  public function ajaxReplaceForm(array $form, FormStateInterface $form_state) {
    // Embed status message into the form.
    $form = ['messages' => ['#type' => 'status_messages']] + $form;
    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = \Drupal::service('renderer');
    // Render the form.
    $output = $renderer->renderRoot($form);

    $response = new AjaxResponse();
    $response->setAttachments($form['#attached']);

    // Replace the form completely and return it.
    return $response->addCommand(new ReplaceCommand('.et_transaction-view-form-' . $this->et_transaction->id(), $output));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  public function showResults(EtTransactionInterface $et_transaction, FormStateInterface $form_state) {
    $account = $this->currentUser();
    switch (TRUE) {
      // The "View results" button, when available, has been clicked.
      case $form_state->get('show_results'):
      return TRUE;

      // The et_transaction is closed.
      case ($et_transaction->isClosed()):
      return TRUE;

      default:
      return FALSE;
    }
  }

  protected function actions(array $form, FormStateInterface $form_state, $et_transaction) {
    $actions = [];

    // Default ajax behavior, use the et_transaction URL for faster submission, this
    // requires that we explicitly provide the ajax_form query argument too in
    // the separate options key, as that replaces all options of the Url object.
    $ajax = [
      'callback' => '::ajaxReplaceForm',
      'url' => $this->et_transaction->toUrl(),
      'options' => ['query' => [FormBuilderInterface::AJAX_FORM_REQUEST => TRUE, 'view_mode' => $form['#view_mode']]]
    ];

    return $actions;
  }

  /**
   * Display a themed et_transaction results.
   *
   * @param \Drupal\expense_tracker\EtTransactionInterface $et_transaction
   *   The et_transaction entity.
   * @param bool $block
   *   (optional) TRUE if a et_transaction should be displayed in a block. Defaults to
   *   FALSE.
   *
   * @return array $output
   */
  function showEtTransactionResults(EtTransactionInterface $et_transaction, $view_mode = 'default', $block = FALSE) {}



  /**
   * Cancel transaction submit function.
   *
   * @param array $form
   *   The previous form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function cancel(array $form, FormStateInterface $form_state) {}

  /**
   * View transaction results submit function.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function result(array $form, FormStateInterface $form_state) {
    $form_state->set('show_results', TRUE);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Back to et_transaction view submit function.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function back(array $form, FormStateInterface $form_state) {
    $form_state->set('show_results', FALSE);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Save a user's transaction submit function.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function save(array $form, FormStateInterface $form_state) {}

  /**
   * Validates the transaction action.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateVote(array &$form, FormStateInterface $form_state) {}

  /**
   * Checks if the current user is allowed to cancel on the given et_transaction.
   *
   * @param \Drupal\expense_tracker\EtTransactionInterface $et_transaction
   *
   * @return bool
   *   TRUE if the user can cancel.
   */
  protected function isCancelAllowed(EtTransactionInterface $et_transaction) {}

}
