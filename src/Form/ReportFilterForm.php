<?php

namespace Drupal\expense_tracker\Form;

use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\expense_tracker\Entity\EtTransaction;

/**
 * Class ReportFilterForm.
 */
class ReportFilterForm extends FormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * Constructs a new ReportFilterForm object.
   */
  public function __construct(
    MessengerInterface $messenger
  ) {
    $this->messenger = $messenger;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ex82_hello_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
   $form_state->setMethod('GET');
   $filter = \Drupal::request()->query->get('filter')??'this_week';

   $route_name = \Drupal::routeMatch()->getRouteName();

   $author = NULL;
   $category = NULL;
   $page_title = '';

   $query_params = \Drupal::request()->query->all();

   if(isset($query_params['author']) && $query_params['author'] != '') {
    $author = $query_params['author'];
    $author = (int) filter_var($author, FILTER_SANITIZE_NUMBER_INT);
    $author = User::load($author);
  }
  if(isset($query_params['category']) && $query_params['category'] != '') {
    $category = $query_params['category'];
    $category = (int) filter_var($category, FILTER_SANITIZE_NUMBER_INT);
    $category = EtTransaction::load($category);
  }

  $request = \Drupal::request();
  if ($route = $request->attributes->get(RouteObjectInterface::ROUTE_OBJECT)) {
    $page_title = \Drupal::service('title_resolver')->getTitle($request, $route);
  }

  $options = array(
   'last_seven_days' => $this->t('Last 7 days'),
   'last_thirty_days' => $this->t('Last 30 days'),
   'this_week' => $this->t('This week'),
   'last_week' => $this->t('Last week'),
   'this_month' => $this->t('This month'),
   'last_month' => $this->t('Last month'),
   'this_quarter' => $this->t('This Quarter'),
   'last_quarter' => $this->t('Last Quarter'),
   'this_year' => $this->t('This year'),
   'last_year' => $this->t('Last year'),
   'all' => $this->t('All')
 );

  $form['details'] = [
    '#type' => 'details',
    '#title' => $this->t('Filter '.$page_title),
    '#open' => true,
    '#description' => $this->t('Filter reports by date range, authored by and category')
  ];

  $form['details']['filter'] = array(
    '#type' => 'select',
    '#title' => t('Analyze income and expense statement'),
    '#options' => $options,
    '#description' => t('Filter financial statements by specific date range'),
    '#default_value' => $filter,
  );

  $form['details']['author'] = [
    '#type' => 'entity_autocomplete',
    '#target_type' => 'user',
    '#default_value' => $author,
    '#title' => $this->t('Authored by'),
    '#size' => 35,

  ];
  $form['details']['category'] = [
    '#type' => 'entity_autocomplete',
    '#target_type' => 'et_transaction',
    '#default_value' => $category,
    '#title' => $this->t('Category'),
    '#size' => 35,

  ];

  $form['details']['submit'] = [
    '#type' => 'submit',
    '#value' => $this->t('Submit'),
    '#attributes' => [
      'class' => [
        'button button-action button--primary',
      ],
    ],
  ];

  $url_object = Url::fromRoute($route_name);
  $form['details']['reset(array)'] = [
    '#type' => 'link',
    '#url' => $url_object,
    '#title' => $this->t('Reset'),
    '#attributes' => [
      'class' => [
        'button js-form-submit form-submit',
      ],
    ],
  ];

  return $form;
}

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}