<?php

namespace Drupal\expense_tracker\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\expense_tracker\Entity\EtTransaction;

/**
 * Form controller for the et_transaction edit forms.
 */
class EtTransactionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $et_transaction = $this->entity;

    if ($et_transaction->isNew()) {
      $title = $this->t('Add new transaction');
    }
    else {
      $title = $this->t('Edit @label', ['@label' => $et_transaction->label()]);
    }
    $form['#title'] = $title;

    $form['#attached'] = ['library' => ['et_transaction/admin']];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // $form_state->setValueForElement($form['title'], '221');
    $et_transaction = $this->buildEntity($form, $form_state);


    // Check for duplicate titles.
    $et_transaction_storage = $this->entityTypeManager->getStorage('et_transaction');
    $result = $et_transaction_storage->getEtTransactionDuplicates($et_transaction);

    foreach ($result as $item) {
      if($et_transaction->transaction_category->value == 'new' && $item->transaction_category->value == 'new') {

        if (strcasecmp($item->label(), $et_transaction->label()) == 0) {
          $form_state->setErrorByName('title', $this->t('A transaction named %transaction already exists. Enter a unique title.', array('%transaction' => $et_transaction->label())));
        }
      }
    }

    $dynamic_required_fields = array();

    if($et_transaction->transaction_category->value == 'existing') {
      $dynamic_required_fields[] = 'category';
    }

    if($et_transaction->repeat->value == '1') {
      $dynamic_required_fields[] = 'repeat_every';
      $dynamic_required_fields[] = 'end_date';
    }

    if($et_transaction->repeat_every->value == 'specific_week_days') {
      $dynamic_required_fields[] = 'specific_week_days';
    }

    if($et_transaction->repeat_every->value == 'specific_month_days') {
      $dynamic_required_fields[] = 'specific_month_days';
    }

    foreach ($dynamic_required_fields as $field_name) {

      $field_value = $et_transaction->{$field_name}->getValue();

      // $field_label = $et_transaction->{$field_name}->label();

      if((is_array($field_value) && empty($field_value)) || (!is_array($field_value) && $field_value == '')) {
        $form_state->setErrorByName($field_name, $this->t('The %transaction field is required.', array('%transaction' => $field_name)));
      }
    }

    if($et_transaction->amount->value < 0) {
      $form_state->setErrorByName('amount', $this->t('The %transaction field  can not be less than 0.', array('%transaction' => $field_name)));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $et_transaction = $this->entity;
    $insert = (bool) $et_transaction->id();
    $transaction_category = $et_transaction->transaction_category->value;

    if($transaction_category == 'existing') {
      if(isset($et_transaction->category->getValue()[0]['target_id'])) {
        $category = $et_transaction->category->getValue()[0]['target_id'];
        $et_transaction->set('parent_category', $category);

        $category = $et_transaction->category->getValue()[0]['target_id'];

        $EtTransaction =  EtTransaction::load($category);
        $title = $EtTransaction->getTitle();
        $et_transaction->set('title', $title);
      }
    } else {
      $et_transaction->set('parent_category', 0);
      $repeat = $et_transaction->repeat->value;
      if($repeat) {
        $parent_transaction_id = $et_transaction->parent_transaction_id->value;
        if($parent_transaction_id > 0) {
         $EtTransaction =  EtTransaction::load($parent_transaction_id);
         if(!$EtTransaction) {
           $et_transaction->set('parent_transaction_id', 0);
         }
       }
     }
   }

   $et_transaction->save();
   if ($insert) {
    $this->messenger()->addMessage($this->t('The transaction %et_transaction has been updated.', array('%et_transaction' => $et_transaction->label())));
  }
  else {
    \Drupal::logger('et_transaction')->notice('EtTransaction %et_transaction added.', array('%et_transaction' => $et_transaction->label(), 'link' => $et_transaction->toLink()->toString()));
    $this->messenger()->addMessage($this->t('The et_transaction %et_transaction has been added.', array('%et_transaction' => $et_transaction->label())));
  }

  $form_state->setRedirect('expense_tracker.et_transaction_list');
}

}
