<?php

namespace Drupal\expense_tracker\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\expense_tracker\EtTransactionInterface;

/**
 * Returns responses for expense_tracker module routes.
 */
class EtTransactionController extends ControllerBase {

  /**
   * Route title callback.
   *
   * @param \Drupal\expense_tracker\EtTransactionInterface $et_transaction
   *   The et_transaction entity.
   *
   * @return string
   *   The et_transaction label.
   */
  public function et_transactionTitle(EtTransactionInterface $et_transaction) {
    return $et_transaction->label();
  }

  public function reports() {

    $route_name = \Drupal::routeMatch()->getRouteName();
    $income_expense_reports = false;
    $theme = 'income_reports';

    $categories_data = array();
    $series_data = array();

    $report_type = 'income';

    if($route_name == 'expense_tracker.income_expense_reports') {
      $income_expense_reports = true;
      $theme = 'income_expense_reports';
      $report_type = 'income_expense';
    } else if($route_name == 'expense_tracker.expense_reports') {
      $report_type = 'expense';
    }

    $filter = \Drupal::request()->get('filter')??'this_week';
    $type = \Drupal::request()->get('type')??'income';
    $author = \Drupal::request()->get('author')??'';
    $category = \Drupal::request()->get('category')??'';

    $report_parameters = array(
      'author' => $author,
      'category' => $category,
    );

    $dates = array();
    $reports_data = array();

    $reports_chart_data['income'] = array();
    $reports_chart_data['expense'] = array();
    $total_income_amount = 0;
    $total_expense_amount = 0;

    switch ($filter) {
      case 'last_seven_days':
    // get last seven days data
      $yesterday = strtotime("-1 day");
      $last_seven_days_from = strtotime("-7 day");
      $datediff = $yesterday - $last_seven_days_from;
      $total_days = round($datediff / (60 * 60 * 24));

      $dates['start'] =  date("Y-M-d", $last_seven_days_from);
      $dates['end'] =  date("Y-M-d", $yesterday);

      $transaction_details = array();
      $reports = array();
      $expense_reports = array();

      if($income_expense_reports) {
        $reports['income'] = $reports['expense'] = array();
        for ($i=0; $i < 7; $i++) {

          $reports_data['categories'][$i] = date("d", strtotime('+'.$i.' days', $last_seven_days_from));

          $start_timestamp = strtotime('+'.$i.' days 12:00 a.m.', $last_seven_days_from);
          $end_timestamp = strtotime('+'.$i.' days 11:59 p.m.', $last_seven_days_from);

          $report_type = 'income';

          $report_parameters['start_timestamp'] = $start_timestamp;
          $report_parameters['end_timestamp'] = $end_timestamp;
          $report_parameters['report_type'] = $report_type;

          $get_total_amounts_income = get_total_amounts($report_parameters);

          if(isset($get_total_amounts_income['transaction_details'])) {
            $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_income['transaction_details'];
          }

          $report_type = 'expense';

          $report_parameters['start_timestamp'] = $start_timestamp;
          $report_parameters['end_timestamp'] = $end_timestamp;
          $report_parameters['report_type'] = $report_type;

          $get_total_amounts_expense = get_total_amounts($report_parameters);

          $income_amount = $get_total_amounts_income['total_amount'];
          $expense_amount = $get_total_amounts_expense['total_amount'];
          $total_income_amount = $total_income_amount + $income_amount;
          $total_expense_amount = $total_expense_amount + $expense_amount;

          $reports_data['series']['income'][$i] = $income_amount;
          $reports_data['series']['expense'][$i] = $expense_amount;

          $reports['income'] = $reports['income'] + $get_total_amounts_income['transaction_details'];
          $reports['expense'] = $reports['expense'] + $get_total_amounts_expense['transaction_details'];


          if(isset($get_total_amounts_expense['transaction_details'])) {
            $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_expense['transaction_details'];
          }



        }
      } else {

       for ($i=0; $i < 7; $i++) {
        $start_timestamp = strtotime('+'.$i.' days 12:00 a.m.', $last_seven_days_from);
        $end_timestamp = strtotime('+'.$i.' days 11:59 p.m.', $last_seven_days_from);

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts = get_total_amounts($report_parameters);
        $reports_data['categories'][$i] = date("d", strtotime('+'.$i.' days', $last_seven_days_from));
        $reports_data['series'][$i] = $get_total_amounts['total_amount'];
        if(isset($get_total_amounts['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts['transaction_details'];
        }

      }

    }

    break;
    case 'last_thirty_days':
    // get last 30 days data
    $last_30 = $last_thirty_days = array();

    if($income_expense_reports) {
      $categories = array();
      for($i = 1; $i <= 30; $i++) {
        $categories[$i] = date("d", strtotime('-'. $i .' days'));

        $start_timestamp = strtotime('-'. $i .' days 12:00 a.m.');
        $end_timestamp = strtotime('-'. $i .' days 11:59 p.m.');
        $report_type = 'income';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_income = get_total_amounts($report_parameters);
        if(isset($get_total_amounts_income['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_income['transaction_details'];
        }
        $report_type = 'expense';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_expense = get_total_amounts($report_parameters);

        $income_amount = $get_total_amounts_income['total_amount'];
        $expense_amount = $get_total_amounts_expense['total_amount'];
        $total_income_amount = $total_income_amount + $income_amount;
        $total_expense_amount = $total_expense_amount + $expense_amount;

        $reports_data['series']['income'][$i] = $income_amount;
        $reports_data['series']['expense'][$i] = $expense_amount;


        if(isset($get_total_amounts_expense['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_expense['transaction_details'];
        }

      }

      $categories = array_reverse($categories);
      // $series = array_reverse($series);

      $reports_data['series']['income'] = array_reverse($reports_data['series']['income']);
      $reports_data['series']['expense'] = array_reverse($reports_data['series']['expense']);
    } else {
      for($i = 1; $i <= 30; $i++) {
        $categories[$i] = date("d", strtotime('-'. $i .' days'));
        $start_timestamp = strtotime('-'. $i .' days 12:00 a.m.');
        $end_timestamp = strtotime('-'. $i .' days 11:59 p.m.');


        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts = get_total_amounts($report_parameters);
        $series[$i] = $get_total_amounts['total_amount'];
        if(isset($get_total_amounts['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts['transaction_details'];
        }
      }

      $categories = array_reverse($categories);
      $series = array_reverse($series);
      $reports_data['series'] = $series;
    }

    $reports_data['categories'] = $categories;

    $dates['start'] =  date("Y-M-d", strtotime('-30 days'));
    $dates['end'] = date("Y-M-d", strtotime('-1 days'));

    break;

    case 'this_week':
    // get this week data
    $now = time();
    $this_week_start = strtotime('last sunday', time());
    $datediff = $now - $this_week_start;

    $total_days = round($datediff / (60 * 60 * 24));
    if($income_expense_reports) {
      for ($i=0; $i <= $total_days; $i++) {
        $categories[$i] = date("M d", strtotime('-'. $i .' days'));
        // $reports_data['series']['income'][$i] = rand(1, 500);
        // $reports_data['series']['expense'][$i] = rand(500, 800);


        $start_timestamp = strtotime('-'. $i .' days 12:00 a.m.');
        $end_timestamp = strtotime('-'. $i .' days 11:59 p.m.');

        $report_type = 'income';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_income = get_total_amounts($report_parameters);

        if(isset($get_total_amounts_income['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_income['transaction_details'];
        }

        $report_type = 'expense';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_expense = get_total_amounts($report_parameters);

        $income_amount = $get_total_amounts_income['total_amount'];
        $expense_amount = $get_total_amounts_expense['total_amount'];
        $total_income_amount = $total_income_amount + $income_amount;
        $total_expense_amount = $total_expense_amount + $expense_amount;

        $reports_data['series']['income'][$i] = $income_amount;
        $reports_data['series']['expense'][$i] = $expense_amount;

        if(isset($get_total_amounts_expense['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_expense['transaction_details'];
        }

      }

      $reports_data['series']['income'] = array_reverse($reports_data['series']['income']);
      $reports_data['series']['expense'] = array_reverse($reports_data['series']['expense']);

      $categories = array_reverse($categories);
      $series = array_reverse($reports_data['series']);

      $reports_data['categories'] = $categories;
      $reports_data['series'] = $series;
    } else {

      for ($i = 0; $i <= $total_days; $i++) {
        $categories[$i] = date("M d", strtotime('-'. $i .' days'));

        // $date_time =  date("Y-m-d H:i:s a", strtotime('-'. $i .' days'));

        $start_timestamp = strtotime('-'. $i .' days 12:00 a.m.');
        $end_timestamp = strtotime('-'. $i .' days 11:59 p.m.');


        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts = get_total_amounts($report_parameters);
        $series[$i] = $get_total_amounts['total_amount'];
        if(isset($get_total_amounts['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts['transaction_details'];
        }
      }

      $categories = array_reverse($categories);
      $series = array_reverse($series);

      $reports_data['categories'] = $categories;
      $reports_data['series'] = $series;
    }

    $dates['start'] =  date("Y-M-d", strtotime('-'. $total_days .' days'));
    $dates['end'] = date("Y-M-d", strtotime('-0 days'));
    break;
    case 'last_week':
    // get lass week data
    $now = time();
    $last_week_start = strtotime('last sunday -7 days');
    $last_week_end = strtotime('last saturday', time());
    $datediff = $last_week_end - $last_week_start;

    $total_days = round($datediff / (60 * 60 * 24));

    // echo date("Y-m-d H:i:s",$last_week_end);
    if($income_expense_reports) {
      for ($i=0; $i <= $total_days; $i++) {
        $reports_data['categories'][$i] = date("M d", strtotime('+'.$i.' days', $last_week_start));


        $start_timestamp = strtotime('+'.$i.' days 12:00 a.m.', $last_week_start);
        $end_timestamp = strtotime('+'.$i.' days 11:59 p.m.', $last_week_start);
        $report_type = 'income';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_income = get_total_amounts($report_parameters);
        if(isset($get_total_amounts_income['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_income['transaction_details'];
        }

        $report_type = 'expense';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_expense = get_total_amounts($report_parameters);
        $income_amount = $get_total_amounts_income['total_amount'];
        $expense_amount = $get_total_amounts_expense['total_amount'];
        $total_income_amount = $total_income_amount + $income_amount;
        $total_expense_amount = $total_expense_amount + $expense_amount;

        $reports_data['series']['income'][$i] = $income_amount;
        $reports_data['series']['expense'][$i] = $expense_amount;


        if(isset($get_total_amounts_expense['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_expense['transaction_details'];
        }

      }
    } else {
      for ($i=0; $i <= $total_days; $i++) {
        $reports_data['categories'][$i] = date("M d", strtotime('+'.$i.' days', $last_week_start));

        $start_timestamp = strtotime('+'. $i .' days 12:00 a.m.', $last_week_start);
        $end_timestamp = strtotime('+'. $i .' days 11:59 p.m.', $last_week_start);


        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts = get_total_amounts($report_parameters);
        $reports_data['series'][$i] = $get_total_amounts['total_amount'];
        if(isset($get_total_amounts['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts['transaction_details'];
        }
      }

    }

    $dates['start'] =  date("Y-M-d", strtotime('+0 days', $last_week_start));
    $dates['end'] =  date("Y-M-d", strtotime('+'.$total_days.' days', $last_week_start));
    break;

    case 'this_month':
    // get this month data
    $now = time();
    $this_month_start = strtotime('first day of this month');
    $today = time();
    $datediff = $today - $this_month_start;

    $total_days = round($datediff / (60 * 60 * 24));

    if($income_expense_reports) {
      for ($i=0; $i <= $total_days; $i++) {
        $reports_data['categories'][$i] = date("M d", strtotime('+'.$i.' days', $this_month_start));

        $start_timestamp = strtotime('+'.$i.' days 12:00 a.m.', $this_month_start);
        $end_timestamp = strtotime('+'.$i.' days 11:59 p.m.', $this_month_start);
        $report_type = 'income';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_income = get_total_amounts($report_parameters);

        if(isset($get_total_amounts_income['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_income['transaction_details'];
        }

        $report_type = 'expense';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_expense = get_total_amounts($report_parameters);
        $income_amount = $get_total_amounts_income['total_amount'];
        $expense_amount = $get_total_amounts_expense['total_amount'];
        $total_income_amount = $total_income_amount + $income_amount;
        $total_expense_amount = $total_expense_amount + $expense_amount;

        $reports_data['series']['income'][$i] = $income_amount;
        $reports_data['series']['expense'][$i] = $expense_amount;


        if(isset($get_total_amounts_expense['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_expense['transaction_details'];
        }

      }
    } else {
      for ($i=0; $i <= $total_days; $i++) {
        $reports_data['categories'][$i] = date("M d", strtotime('+'.$i.' days', $this_month_start));

        $start_timestamp = strtotime('+'. $i .' days 12:00 a.m.', $this_month_start);
        $end_timestamp = strtotime('+'. $i .' days 11:59 p.m.', $this_month_start);


        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts = get_total_amounts($report_parameters);
        $reports_data['series'][$i] = $get_total_amounts['total_amount'];
        if(isset($get_total_amounts['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts['transaction_details'];
        }

      }

    }

    $dates['start'] =  date("Y-M-d", strtotime('+0 days', $this_month_start));
    $dates['end'] =  date("Y-M-d", strtotime('+'.$total_days.' days', $this_month_start));
    break;

    case 'last_month':
     // get this month data
    $now = time();
    $last_month_start = strtotime('first day of last month');
    $last_month_end = strtotime('last day of last month');
    $datediff = $last_month_end - $last_month_start;

    $total_days = round($datediff / (60 * 60 * 24));

    // echo date("Y-m-d H:i:s",$last_month_end);
    if($income_expense_reports) {
      for ($i=0; $i <= $total_days; $i++) {
        $reports_data['categories'][$i] = date("d", strtotime('+'.$i.' days', $last_month_start));

        $start_timestamp = strtotime('+'.$i.' days 12:00 a.m.', $last_month_start);
        $end_timestamp = strtotime('+'.$i.' days 11:59 p.m.', $last_month_start);
        $report_type = 'income';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_income = get_total_amounts($report_parameters);
        if(isset($get_total_amounts_income['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_income['transaction_details'];
        }

        $report_type = 'expense';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_expense = get_total_amounts($report_parameters);
        $income_amount = $get_total_amounts_income['total_amount'];
        $expense_amount = $get_total_amounts_expense['total_amount'];
        $total_income_amount = $total_income_amount + $income_amount;
        $total_expense_amount = $total_expense_amount + $expense_amount;

        $reports_data['series']['income'][$i] = $income_amount;
        $reports_data['series']['expense'][$i] = $expense_amount;


        if(isset($get_total_amounts_expense['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_expense['transaction_details'];
        }

      }
    } else {
      for ($i=0; $i <= $total_days; $i++) {
        $reports_data['categories'][$i] = date("d", strtotime('+'.$i.' days', $last_month_start));

        $start_timestamp = strtotime('+'. $i .' days 12:00 a.m.', $last_month_start);
        $end_timestamp = strtotime('+'. $i .' days 11:59 p.m.', $last_month_start);


        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts = get_total_amounts($report_parameters);
        $reports_data['series'][$i] = $get_total_amounts['total_amount'];
        if(isset($get_total_amounts['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts['transaction_details'];
        }
      }

    }
    $dates['start'] =  date("Y-M-d", strtotime('+0 days', $last_month_start));
    $dates['end'] =  date("Y-M-d", strtotime('+'.$total_days.' days', $last_month_start));
    break;
    case 'this_quarter':
     // get this this_quarter data
    $current_quarter = ceil(date('n') / 3);
    $this_quarter_start =  strtotime(date('Y') . '-' . (($current_quarter * 3) - 2) . '-1');
    $this_quarter_end = strtotime(date('Y') . '-' . (($current_quarter * 3)) . '-1');

    $datediff = $this_quarter_end - $this_quarter_start;

    $total_months = round($datediff / (60 * 60 * 24*30));

    $current_quarter = ceil(date('n') / 3);
    $first_date = date('Y-m-d', strtotime(date('Y') . '-' . (($current_quarter * 3) - 2) . '-1'));
    $last_date = date('Y-m-t', strtotime(date('Y') . '-' . (($current_quarter * 3)) . '-1'));
    $first_date_timestamp = strtotime(date('Y') . '-' . (($current_quarter * 3) - 2) . '-1');

    if($income_expense_reports) {
      for ($i=0; $i < 3; $i++) {
        $reports_data['categories'][$i] = date("M", strtotime('+'.$i.' months', $first_date_timestamp));

        $start_timestamp = strtotime('+'.$i.' months 12:00 a.m.', $first_date_timestamp);
        $end_timestamp = strtotime('+'.$i.' months last day of this month 11:59 p.m.', $first_date_timestamp);

        $report_type = 'income';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_income = get_total_amounts($report_parameters);

        if(isset($get_total_amounts_income['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_income['transaction_details'];
        }

        $report_type = 'expense';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_expense = get_total_amounts($report_parameters);
        $income_amount = $get_total_amounts_income['total_amount'];
        $expense_amount = $get_total_amounts_expense['total_amount'];
        $total_income_amount = $total_income_amount + $income_amount;
        $total_expense_amount = $total_expense_amount + $expense_amount;

        $reports_data['series']['income'][$i] = $income_amount;
        $reports_data['series']['expense'][$i] = $expense_amount;



        if(isset($get_total_amounts_expense['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_expense['transaction_details'];
        }

      }

    } else {

      for ($i=0; $i < 3; $i++) {
        $reports_data['categories'][$i] = date("M", strtotime('+'.$i.' months', $first_date_timestamp));

        $start_timestamp = strtotime('+'. $i .' months 12:00 a.m.', $first_date_timestamp);
        $end_timestamp = strtotime('+'. $i .' months last day of this month 11:59 p.m.', $first_date_timestamp);


        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts = get_total_amounts($report_parameters);
        $reports_data['series'][$i] = $get_total_amounts['total_amount'];
        if(isset($get_total_amounts['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts['transaction_details'];
        }
      }

    }
    $dates['start'] =  date("Y-M-d", strtotime('+0 days', $this_quarter_start));
    $end_timestamp = strtotime('+'.$total_months.' months', $this_quarter_start);
    $dates['end'] = date("Y-M-d", strtotime('last day of this month', $end_timestamp));
    break;
    case 'last_quarter':
     // get this last_quarter data
    $last_quarter_count = ceil(date('n') / 3)-1;
    $last_quarter_start = strtotime(date('Y') . '-' . (($last_quarter_count * 3) - 2) . '-1');
    $last_quarter_end = strtotime(date('Y') . '-' . (($last_quarter_count * 3)) . '-1');

    $datediff = $last_quarter_end - $last_quarter_start;

    $total_days = round($datediff / (60 * 60 * 24*30));

    // echo date("Y-m-d H:i:s",$last_quarter_start);
    if($income_expense_reports) {
      for ($i=0; $i < 3; $i++) {
        $reports_data['categories'][$i] = date("M", strtotime('+'.$i.' months', $last_quarter_start));

        $start_timestamp = strtotime('+'.$i.' months 12:00 a.m.', $last_quarter_start);
        $end_timestamp = strtotime('+'.$i.' months last day of this month 11:59 p.m.', $last_quarter_start);

        $report_type = 'income';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_income = get_total_amounts($report_parameters);
        if(isset($get_total_amounts_income['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_income['transaction_details'];
        }
        $report_type = 'expense';

        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts_expense = get_total_amounts($report_parameters);
        $income_amount = $get_total_amounts_income['total_amount'];
        $expense_amount = $get_total_amounts_expense['total_amount'];
        $total_income_amount = $total_income_amount + $income_amount;
        $total_expense_amount = $total_expense_amount + $expense_amount;

        $reports_data['series']['income'][$i] = $income_amount;
        $reports_data['series']['expense'][$i] = $expense_amount;


        if(isset($get_total_amounts_expense['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_expense['transaction_details'];
        }
      }
    } else {
      for ($i=0; $i < 3; $i++) {
        $reports_data['categories'][$i] = date("M", strtotime('+'.$i.' months', $last_quarter_start));

        $start_timestamp = strtotime('+'. $i .' months 12:00 a.m.', $last_quarter_start);
        $end_timestamp = strtotime('+'. $i .' months last day of this month 11:59 p.m.', $last_quarter_start);


        $report_parameters['start_timestamp'] = $start_timestamp;
        $report_parameters['end_timestamp'] = $end_timestamp;
        $report_parameters['report_type'] = $report_type;

        $get_total_amounts = get_total_amounts($report_parameters);
        $reports_data['series'][$i] = $get_total_amounts['total_amount'];
        if(isset($get_total_amounts['transaction_details'])) {
          $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts['transaction_details'];
        }
      }

    }
    $dates['start'] =  date("Y-M-d", strtotime('+0 months', $last_quarter_start));
    $end_timestamp =   strtotime('+'.$total_days.' months', $last_quarter_start);
    $dates['end'] =   date("Y-M-d", strtotime('last day of this month', $end_timestamp));
    break;
    case 'this_year':
     // get this_year data
    $this_year_start = strtotime("first day of january this year");
    $this_year_end =  strtotime("last day of december this year");

    $datediff = $this_year_end - $this_year_start;

    $total_months = round($datediff / (60 * 60 * 24*30));

    // echo date("Y-m-d H:i:s",$this_year_start);
    if($income_expense_reports) {
      for ($i=0; $i <= $total_months; $i++) {
       $reports_data['categories'][$i] = date("M", strtotime('+'.$i.' months', $this_year_start));

       $start_timestamp = strtotime('+'.$i.' month first day of this year 12:00 a.m.', $this_year_start);
       $end_timestamp = strtotime('+'.$i.' month last day of this year 11:59 p.m.', $this_year_start);
       $report_type = 'income';

       $report_parameters['start_timestamp'] = $start_timestamp;
       $report_parameters['end_timestamp'] = $end_timestamp;
       $report_parameters['report_type'] = $report_type;

       $get_total_amounts_income = get_total_amounts($report_parameters);
       if(isset($get_total_amounts_income['transaction_details'])) {
        $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_income['transaction_details'];
      }
      $report_type = 'expense';

      $report_parameters['start_timestamp'] = $start_timestamp;
      $report_parameters['end_timestamp'] = $end_timestamp;
      $report_parameters['report_type'] = $report_type;

      $get_total_amounts_expense = get_total_amounts($report_parameters);
      $income_amount = $get_total_amounts_income['total_amount'];
      $expense_amount = $get_total_amounts_expense['total_amount'];
      $total_income_amount = $total_income_amount + $income_amount;
      $total_expense_amount = $total_expense_amount + $expense_amount;

      $reports_data['series']['income'][$i] = $income_amount;
      $reports_data['series']['expense'][$i] = $expense_amount;


      if(isset($get_total_amounts_expense['transaction_details'])) {
        $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_expense['transaction_details'];
      }



    }
  } else {
    for ($i=0; $i < $total_months; $i++) {
      $reports_data['categories'][$i] = date("M", strtotime('+'.$i.' months', $this_year_start));

      $start_timestamp = strtotime('+'. $i .' month first day of this year 12:00 a.m.', $this_year_start);
      $end_timestamp = strtotime('+'. $i .'month last day of this year 11:59 p.m.', $this_year_start);


      $report_parameters['start_timestamp'] = $start_timestamp;
      $report_parameters['end_timestamp'] = $end_timestamp;
      $report_parameters['report_type'] = $report_type;

      $get_total_amounts = get_total_amounts($report_parameters);

      $reports_data['series'][$i] = $get_total_amounts['total_amount'];
      if(isset($get_total_amounts['transaction_details'])) {
        $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts['transaction_details'];
      }
    }
  }
  $dates['start'] =  date("Y-M-d", strtotime('first day of this year', $this_year_start));
  $dates['end'] =  date("Y-M-d", strtotime('last day of this month', $this_year_end));
  break;
  case 'last_year':

     // get last_year data
  $last_year_start = strtotime("first day of january last year");
  $last_year_end =  strtotime("last day of december last year");

  $datediff = $last_year_end - $last_year_start;

  $total_months = round($datediff / (60 * 60 * 24*30));

    // echo date("Y-m-d H:i:s",$last_year_start);
  if($income_expense_reports) {
    for ($i=0; $i <= $total_months; $i++) {
      $reports_data['categories'][$i] = date("M", strtotime('+'.$i.' months', $last_year_start));

      $start_timestamp = strtotime('+'.$i.' month first day of this year 12:00 a.m.', $last_year_start);
      $end_timestamp = strtotime('+'.$i.' month last day of this year 11:59 p.m.', $last_year_start);
      $report_type = 'income';

      $report_parameters['start_timestamp'] = $start_timestamp;
      $report_parameters['end_timestamp'] = $end_timestamp;
      $report_parameters['report_type'] = $report_type;

      $get_total_amounts_income = get_total_amounts($report_parameters);
      if(isset($get_total_amounts_income['transaction_details'])) {
        $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_income['transaction_details'];
      }
      $report_type = 'expense';

      $report_parameters['start_timestamp'] = $start_timestamp;
      $report_parameters['end_timestamp'] = $end_timestamp;
      $report_parameters['report_type'] = $report_type;

      $get_total_amounts_expense = get_total_amounts($report_parameters);
      $income_amount = $get_total_amounts_income['total_amount'];
      $expense_amount = $get_total_amounts_expense['total_amount'];
      $total_income_amount = $total_income_amount + $income_amount;
      $total_expense_amount = $total_expense_amount + $expense_amount;

      $reports_data['series']['income'][$i] = $income_amount;
      $reports_data['series']['expense'][$i] = $expense_amount;

      if(isset($get_total_amounts_expense['transaction_details'])) {
        $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_expense['transaction_details'];
      }
    }
  } else {
    for ($i=0; $i < $total_months; $i++) {
      $reports_data['categories'][$i] = date("M", strtotime('+'.$i.' months', $last_year_start));
      $start_timestamp = strtotime('+'. $i .' month first day of this month 12:00 a.m.', $last_year_start);
      $end_timestamp = strtotime('+'. $i .'month last day of this month 11:59 p.m.', $last_year_start);

      $report_parameters['start_timestamp'] = $start_timestamp;
      $report_parameters['end_timestamp'] = $end_timestamp;
      $report_parameters['report_type'] = $report_type;

      $get_total_amounts = get_total_amounts($report_parameters);
      $reports_data['series'][$i] = $get_total_amounts['total_amount'];
      if(isset($get_total_amounts['transaction_details'])) {
        $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts['transaction_details'];
      }
    }

  }
  $dates['start'] =  date("Y-M-d", $this_year_start);
  $dates['end'] =  date("Y-M-d", $this_year_end);
  break;

  default:

  // get this_year data
  $this_year_start = strtotime("-0 year first day of january");
  $this_year_end =  strtotime("last day of this year 11:59 p.m.");

  $start_date = strtotime("-10 year first day of january");

  $j = 0;
  for ($i=10; $i >= 0 ; $i--) {
    if($income_expense_reports) {
      $reports_data['categories'][$j] = date("Y", strtotime('-'.$i.' year', $this_year_start));
      $start_timestamp = strtotime('-'.$i.' year first day of this year 12:00 a.m.', $this_year_start);
      $end_timestamp = strtotime('-'.$i.' year last day of December this year 11:59 p.m.', $this_year_start);
      $report_type = 'income';

      $report_parameters['start_timestamp'] = $start_timestamp;
      $report_parameters['end_timestamp'] = $end_timestamp;
      $report_parameters['report_type'] = $report_type;

      $get_total_amounts_income = get_total_amounts($report_parameters);
      if(isset($get_total_amounts_income['transaction_details'])) {
        $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_income['transaction_details'];
      }
      $report_type = 'expense';

      $report_parameters['start_timestamp'] = $start_timestamp;
      $report_parameters['end_timestamp'] = $end_timestamp;
      $report_parameters['report_type'] = $report_type;

      $get_total_amounts_expense = get_total_amounts($report_parameters);
      $income_amount = $get_total_amounts_income['total_amount'];
      $expense_amount = $get_total_amounts_expense['total_amount'];
      $total_income_amount = $total_income_amount + $income_amount;
      $total_expense_amount = $total_expense_amount + $expense_amount;

      $reports_data['series']['income'][$j] = $income_amount;
      $reports_data['series']['expense'][$j] = $expense_amount;

      if(isset($get_total_amounts_expense['transaction_details'])) {
        $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts_expense['transaction_details'];
      }

    } else {
      $reports_data['categories'][$j] = date("Y", strtotime('-'.$i.' year', $this_year_start));
      $start_timestamp = strtotime('-'. $i .' year first day of this year 12:00 a.m.', $this_year_start);
      $end_timestamp = strtotime('-'. $i .' year last day of December this year 11:59 p.m.', $this_year_start);


      $report_parameters['start_timestamp'] = $start_timestamp;
      $report_parameters['end_timestamp'] = $end_timestamp;
      $report_parameters['report_type'] = $report_type;

      $get_total_amounts = get_total_amounts($report_parameters);

      $reports_data['series'][$j] = $get_total_amounts['total_amount'];
      if(isset($get_total_amounts['transaction_details'])) {
        $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts['transaction_details'];
      }
    }
    $j++;
  }


  $dates['start'] =  date("Y-M-d", $start_date);
  $dates['end'] =  date("Y-M-d", $this_year_end);
  break;
}

if($income_expense_reports) {
  $report_types = ['income', 'expense'];

  foreach ($report_types as $report_type) {
    foreach ($reports_chart_data[$report_type] as $key => $data) {

      if(isset($categories_data[$report_type]) && in_array($data['title'], $categories_data[$report_type])) {

        $key_index = array_search($data['title'], $categories_data[$report_type]);

        $series_data[$report_type][$key_index] = $series_data[$report_type][$key_index] + $data['amount'];
      } else {
        $categories_data[$report_type][] = $data['title'];
        $series_data[$report_type][] = $data['amount'];
      }
    }
  }

} else {
  $reports_chart_data[$report_type] = $reports_chart_data[$report_type] + $get_total_amounts['transaction_details'];

  if(isset($reports_chart_data[$report_type])) {
    foreach ($reports_chart_data[$report_type] as $key => $data) {

      if(in_array($data['title'], $categories_data)) {
        $key_index = array_search($data['title'], $categories_data);
        $series_data[$key_index] = $series_data[$key_index] + $data['amount'];
      } else {
        $categories_data[] = $data['title'];
        $series_data[] = $data['amount'];
      }
    }
  }
}
$tabs_array = array(
  'last_seven_days' => array(
    'title' => t('Last 7 days'),
    'class' => 'non-active'
  ),
  'last_thirty_days' => array(
    'title' => t('Last 30 days'),
    'class' => 'non-active'
  ),
  'this_week' => array(
    'title' => t('This week'),
    'class' => 'non-active'
  ),
  'last_week' => array(
    'title' => t('Last week'),
    'class' => 'non-active'
  ),
  'this_month' => array(
    'title' => t('This month'),
    'class' => 'non-active'
  ),
  'last_month' => array(
    'title' => t('Last month'),
    'class' => 'non-active'
  ),
  'this_quarter' => array(
    'title' => t('This Quarter'),
    'class' => 'non-active'
  ),
  'last_quarter' => array(
    'title' => t('Last Quarter'),
    'class' => 'non-active'
  ),
  'this_year' => array(
    'title' => t('This year'),
    'class' => 'non-active'
  ),
  'last_year' => array(
    'title' => t('Last year'),
    'class' => 'non-active'
  ),
  'all' => array(
    'title' => t('All'),
    'class' => 'non-active'
  )
);


$reports_data['title'] = t('Last 7 days');
if($filter) {
  $tabs_array[$filter]['class'] = 'active';
}

$reports_data['title'] = $tabs_array[$filter]['title'];
$reports_data['id'] = $filter;

$other_data['dates'] =$dates;
$other_data['report_type'] =$report_type;
$other_data['categories'] =$categories_data;
$other_data['series'] =$series_data;
$other_data['filter'] =$filter;

$other_data['total_amounts'] = array(
  'total_income_amount' => $total_income_amount,
  'total_expense_amount' => $total_expense_amount,
);

$submitImportForm = \Drupal::formBuilder()->getForm('Drupal\expense_tracker\Form\ReportFilterForm');

$build[]= array(
  '#type' => 'markup',
  '#markup' => \Drupal::service('renderer')->render($submitImportForm),
);

// dd($reports_data);

$build[] = [
  '#theme' => $theme,
  '#reports_data' => $reports_data,
  '#tabs' => $tabs_array,
  '#other_data' => $other_data,
];
$build['#attached']['drupalSettings']['layout_data'] =  [];
$build['#attached']['library'][] = 'expense_tracker/expense_tracker.reports';

return $build;


}

}
