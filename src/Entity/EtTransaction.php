<?php

namespace Drupal\expense_tracker\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\expense_tracker\EtTransactionInterface;
use Drupal\user\UserInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines the et_transaction entity class.
 *
 * @ContentEntityType(
 *   id = "et_transaction",
 *   label = @Translation("Transaction"),
 *   handlers = {
 *     "access" = "\Drupal\expense_tracker\EtTransactionAccessControlHandler",
 *     "storage" = "Drupal\expense_tracker\EtTransactionStorage",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "list_builder" = "Drupal\expense_tracker\EtTransactionListBuilder",
 *     "view_builder" = "Drupal\expense_tracker\EtTransactionViewBuilder",
 *     "views_data" = "Drupal\expense_tracker\EtTransactionViewData",
 *     "form" = {
 *       "default" = "Drupal\expense_tracker\Form\EtTransactionForm",
 *       "edit" = "Drupal\expense_tracker\Form\EtTransactionForm",
 *       "delete" = "Drupal\expense_tracker\Form\EtTransactionDeleteForm",
 *       "delete_transaction" = "Drupal\expense_tracker\Form\EtTransactionDeleteForm",
 *       "delete_items" = "Drupal\expense_tracker\Form\EtTransactionItemsDeleteForm",
 *     }
 *   },
 *   links = {
 *     "canonical" = "/expense_tracker/{et_transaction}",
 *     "edit-form" = "/expense_tracker/{et_transaction}/edit",
 *     "delete-form" = "/expense_tracker/{et_transaction}/delete"
 *   },
 *   base_table = "et_transaction",
 *   data_table = "et_transaction_field_data",
 *   admin_permission = "administer et_transactions",
 *   field_ui_base_route = "expense_tracker.settings",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode"
 *   }
 * )
 */
class EtTransaction extends ContentEntityBase implements EtTransactionInterface {

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreated($created) {
    $this->set('created', $created);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreated() {
    return $this->get('created')->value;
  }
  /**
   * {@inheritdoc}
   */
  public function getAmount() {
    return $this->get('amount')->value;
  }
  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->get('id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getRuntime() {
    return $this->get('runtime')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRuntime($runtime) {
    $this->set('runtime', $runtime);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentCategory($parent_category) {
    $this->set('parent_category', $parent_category);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentCategory() {
    $this->get('parent_category')->value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAnonymousTransactionAllow() {
    return $this->get('anonymous_transaction_allow')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAnonymousTransactionAllow($anonymous_transaction_allow) {
    $this->set('anonymous_transaction_allow', $anonymous_transaction_allow);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelTransactionAllow() {
    return $this->get('cancel_transaction_allow')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCancelTransactionAllow($cancel_transaction_allow) {
    $this->set('cancel_transaction_allow', $cancel_transaction_allow);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getResultTransactionAllow() {
    return $this->get('result_transaction_allow')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setResultTransactionAllow($result_transaction_allow) {
    $this->set('result_transaction_allow', $result_transaction_allow);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isOpen() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isClosed() {
    // return (bool) $this->get('status')->value == 0;
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function close() {
    return $this->set('status', 0);
  }

  /**
   * {@inheritdoc}
   */
  public function open() {
    return $this->set('status', 1);
  }

  /**
   * {@inheritdoc}
   */
  public function get_current_datetime() {
    $current_time = DrupalDateTime::createFromTimestamp(time());
    return $current_time->format(DateTimeItem::DATETIME_STORAGE_FORMAT);
  }
  /**
   * {@inheritdoc}
   */
  // public function get_end_of_month_datetime() {
  //   $timestamp = time();
  //   $onemonths_fromnow = strtotime('+1 month', $timestamp);

  //   $current_time = DrupalDateTime::createFromTimestamp($onemonths_fromnow);
  //   return $current_time->format(DateTimeItem::DATETIME_STORAGE_FORMAT);
  // }
  /**
   * {@inheritdoc}
   */
  public static function get_edate() {
    $timestamp = time();
    $onemonths_fromnow = strtotime('+1 months', $timestamp);
    return $onemonths_fromnow;
  }
  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('EtTransaction ID'))
    ->setDescription(t('The ID of the et_transaction.'))
    ->setReadOnly(TRUE)
    ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
    ->setLabel(t('UUID'))
    ->setDescription(t('The et_transaction UUID.'))
    ->setReadOnly(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Title'))
    ->setDescription(t('The transaction(expense/income) title.'))
    ->setRequired(TRUE)
    ->setTranslatable(TRUE)
    ->setSetting('max_length', 255)
    ->setDisplayOptions('form', array(
      'type' => 'string_textfield',
      'weight' => 1,
    ))
    ->setDisplayOptions('view', [
      'type' => 'string',
      'weight' => 1,
      'label' => 'above',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['transaction_category'] = BaseFieldDefinition::create('list_string')
    ->setLabel(t('Translation Category'))
    ->setSettings([
      'allowed_values' => [
        'new' => 'New Transaction',
        'existing' => 'Select existing',
      ],
    ])
    ->setDescription(t('Select the translation category.'))
    ->setRequired(TRUE)
    ->setDefaultValue('new')
    ->setTranslatable(FALSE)
    ->setDisplayOptions('form', [
      'type' => 'options_select',
      'settings' => [],
      'weight' => 2,
    ])
    ->setDisplayOptions('view', [
      'type' => 'string',
      'weight' => 2,
      'label' => 'above',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['category'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Category'))
    ->setSetting('target_type', 'et_transaction')
    ->setDescription(t('Select the existing category(Existing transaction)'))
    ->setRequired(FALSE)
    ->setTranslatable(FALSE)
    ->setCardinality(1)
    ->setSetting('max_length', 255)
    ->setDisplayOptions('form', [
      'type' => 'options_select',
      'weight' => 3,
    ])
    ->setDisplayOptions('view', [
      'type' => 'string',
      'weight' => 3,
      'label' => 'above',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['amount'] = BaseFieldDefinition::create('float')
    ->setLabel(t('Amount'))
    ->setDescription(t('Amount'))
    ->setRequired(TRUE)
    // ->setSettings(array(
    //   'precision' => 5,
    //   'scale' => 2,
    //   'max_length' => 11
    // ))
    ->setDisplayOptions('view', array(
      'label' => 'above',
      'type' => 'number_decimal',
      'weight' => 4,
    ))
    ->setDisplayOptions('form', array(
      'type' => 'number',
      'weight' => 4,
    ))
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['date'] = BaseFieldDefinition::create('created')
    ->setLabel(t('Date of transaction'))
    ->setDescription('')
    ->setRequired(TRUE)
    ->setSettings([
      'datetime_type' => 'datetime'
    ])
    ->setDisplayOptions('form', [
      'type' => 'datetime_timestamp',
      'weight' => 5,
    ])
    ->setDisplayOptions('view', [
      'type' => 'string',
      'weight' => 5,
      'label' => 'above',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['transaction_type'] = BaseFieldDefinition::create('list_string')
    ->setLabel(t('Transaction Type'))
    ->setSettings([
      'allowed_values' => [
        'expense' => 'Expense',
        'income' => 'Income',
      ],
    ])
    ->setDescription(t('Select the translation type.'))
    ->setRequired(TRUE)
    ->setDefaultValue('expense')
    ->setTranslatable(FALSE)
    ->setDisplayOptions('form', [
      'type' => 'options_select',
      'settings' => [],
      'weight' => 6,
    ])
    ->setDisplayOptions('view', [
      'type' => 'string',
      'weight' => 6,
      'label' => 'above',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['repeat'] = BaseFieldDefinition::create('boolean')
    ->setLabel(t('Repeat'))
    ->setDescription(t('Repeat the transaction'))
    ->setDefaultValue(0)
    ->setDisplayOptions('form', array(
      'type' => 'boolean_checkbox',
      'settings' => array(
        'display_label' => TRUE,
      ),
      'weight' => 7,
    ))
    ->setDisplayOptions('view', [
      'type' => 'string',
      'weight' => 7,
      'label' => 'above',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['repeat_every'] = BaseFieldDefinition::create('list_string')
    ->setLabel(t('Repeat every'))
    ->setSettings([
      'allowed_values' => [
        'day' => '1 day',
        'week' => '1 week',
        'month' => '1 month',
        'year' => '1 year',
        'working_days' => 'Working days(Mon-Fri)',
        'specific_week_days' => 'Specific days of the week',
        'specific_month_days' => 'Specific days of the month',
      ],
    ])
    ->setDescription(t('Select the translation repeat type.'))
    ->setRequired(False)
    ->setTranslatable(FALSE)
    ->setDisplayOptions('form', [
      'type' => 'options_select',
      'settings' => [],
      'weight' => 8,
    ])
    ->setDisplayOptions('view', [
      'type' => 'string',
      'weight' => 8,
      'label' => 'above',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['specific_week_days'] = BaseFieldDefinition::create('list_string')
    ->setLabel(t('Specific days of the week'))
    ->setSettings([
      'allowed_values' => [
        '7' => 'Sunday',
        '1' => 'Monday',
        '2' => 'Tuesday',
        '3' => 'Wednesday',
        '4' => 'Thursday',
        '5' => 'Friday',
        '6' => 'Saturday',
      ],
    ])
    ->setDescription(t('Select the translation repeat on specific days of the week.'))
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
    ->setRequired(False)
    ->setTranslatable(FALSE)
    ->setDisplayOptions('form', [
      'type' => 'options_select',
      'settings' => [],
      'weight' => 9,
    ])
    ->setDisplayOptions('view', [
      'type' => 'string',
      'weight' => 9,
      'label' => 'above',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $allowed_days = [];
    for ($i=1; $i <= 31; $i++) {
      $allowed_days[$i] = $i;
    }

    $fields['specific_month_days'] = BaseFieldDefinition::create('list_string')
    ->setLabel(t('Specific days of the month'))
    ->setSettings([
      'allowed_values' => $allowed_days,
    ])
    ->setDescription(t('Select the translation repeat on specific days of the month.'))
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
    ->setRequired(False)
    ->setTranslatable(FALSE)
    ->setDisplayOptions('form', [
      'type' => 'options_select',
      'settings' => [],
      'weight' => 10,
    ])
    ->setDisplayOptions('view', [
      'type' => 'string',
      'weight' => 10,
      'label' => 'above',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['end_date'] = BaseFieldDefinition::create('timestamp')
    ->setLabel(t('Repeat end date'))
    ->setDescription(t('Last time feed was checked for new items, as Unix timestamp.<br>* Please make sure the cron run every day for the repeat functionality.'))
    ->setRequired(TRUE)
    ->setSettings([
      'datetime_type' => 'datetime'
    ])
    ->setDefaultValueCallback('Drupal\expense_tracker\Entity\EtTransaction::get_edate')
    ->setDisplayOptions('form', [
      'type' => 'datetime_timestamp',
      'weight' => 11,
    ])
    ->setDisplayOptions('view', [
      'type' => 'string',
      'weight' => 11,
      'label' => 'above',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['note'] = BaseFieldDefinition::create('text_long')
    ->setLabel(t('Note'))
    ->setDescription(t('The translation note.'))
    ->setSettings(array(
      'max_length'  => 1024
    ))
    ->setDisplayOptions('form', array(
      'type'   => 'text_format',
      'weight' => 12
    ))
    ->setDisplayOptions('view', [
      'type' => 'string',
      'weight' => 12,
      'label' => 'above',
    ])
    ->setRequired(FALSE)
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Author'))
    ->setDescription(t('The transaction author for related person for the transaction.'))
    ->setSetting('target_type', 'user')
    ->setTranslatable(TRUE)
    ->setRequired(TRUE)
    ->setDefaultValueCallback('Drupal\expense_tracker\Entity\EtTransaction::getCurrentUserId')
    ->setDisplayOptions('form', array(
      'type' => 'entity_reference_autocomplete',
      'weight' => 13,
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'size' => '60',
        'autocomplete_type' => 'tags',
        'placeholder' => '',
      ),
    ))
    ->setDisplayOptions('view', [
      'type' => 'string',
      'weight' => 12,
      'label' => 'above',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['created_type'] = BaseFieldDefinition::create('created')
    ->setLabel(t('Created type'))
    ->setDescription(t('When the et_transaction was created, as a Unix timestamp.'));

    $fields['created_type'] = BaseFieldDefinition::create('list_string')
    ->setLabel(t('Created type'))
    ->setSettings([
      'allowed_values' => [
        'manual' => 'Manual',
        'automatic' => 'Automatic',
      ],
    ])
    ->setDescription(t('How the transaction was created.'))
    ->setDefaultValue('manual')
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['parent_category'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Parent category'))
    ->setDescription(t('Parent category id if the current transaction was created with existing category'))
    ->setDefaultValue(0)
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['parent_transaction_id'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Parent transaction id'))
    ->setDescription(t('Parent transaction id if the current transaction was created with reapeat.'))
    ->setDefaultValue(0)
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['last_transaction_time'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Last transaction timestamp'))
    ->setDescription(t('Last transaction timestamp when the repeat was created.'))
    ->setDefaultValue(0)
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
    ->setLabel(t('Language code'))
    ->setDescription(t('The et_transaction language code.'));

    $fields['created'] = BaseFieldDefinition::create('created')
    ->setLabel(t('Created'))
    ->setDescription(t('When the et_transaction was created, as a Unix timestamp.'))
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return array(\Drupal::currentUser()->id());
  }

  /**
   *
   * {@inheritdoc}
   */
  public static function sort($a, $b) {
    return strcmp($a->label(), $b->label());
  }


  /**
   * @todo: Refactor - doesn't belong here.
   *
   * @return mixed
   */
  public function hasUserTransactiond() {
    /** @var \Drupal\et_transaction\EtTransactionTransactionStorage $transaction_storage */
    $transaction_storage = \Drupal::service('expense_tracker_transaction.storage');
    return $transaction_storage->getUserTransaction($this);
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    $options = array();
    if (count($this->choice)) {
      foreach ($this->choice as $choice_item) {
        $options[$choice_item->target_id] = \Drupal::service('entity.repository')->getTranslationFromContext($choice_item->entity, $this->language()->getId())->label();
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptionValues() {
    $options = array();
    if (count($this->choice)) {
      foreach ($this->choice as $choice_item) {
        $options[$choice_item->target_id] = 1;
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!empty($this->choice)) {
      foreach ($this->choice as $choice_item) {
        if ($choice_item->entity && $choice_item->entity->needsSaving()) {
          $choice_item->entity->save();
          $choice_item->target_id = $choice_item->entity->id();
        }
      }
    }

    // Delete no longer referenced choices.
    if (!$this->isNew()) {
      $original_choices = [];
      foreach ($this->original->choice as $choice_item) {
        $original_choices[] = $choice_item->target_id;
      }

      $current_choices = [];
      foreach ($this->choice as $choice_item) {
        $current_choices[] = $choice_item->target_id;
      }

      $removed_choices = array_diff($original_choices, $current_choices);
      if ($removed_choices) {
        \Drupal::service('expense_tracker_transaction.storage')->deleteChoicesTransactions($removed_choices);
        $storage = \Drupal::entityTypeManager()->getStorage('et_transaction_choice');
        $storage->delete($storage->loadMultiple($removed_choices));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    // parent::postDelete($storage, $entities);

    // // Delete transactions.
    // foreach ($entities as $entity) {
    //   $storage->deleteTransactions($entity);
    // }

    // // Delete referenced choices.
    // $choices = [];
    // foreach ($entities as $entity) {
    //   $choices = array_merge($choices, $entity->choice->referencedEntities());
    // }
    // if ($choices) {
    //   \Drupal::entityTypeManager()->getStorage('et_transaction_choice')->delete($choices);
    // }
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactions() {
    /** @var \Drupal\et_transaction\EtTransactionTransactionStorage $transaction_storage */
    $transaction_storage = \Drupal::service('expense_tracker_transaction.storage');
    return $transaction_storage->getTransactions($this);
  }

}
