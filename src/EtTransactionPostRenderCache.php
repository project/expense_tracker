<?php

namespace Drupal\expense_tracker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Defines a service for et_transaction post render cache callbacks.
 */
class EtTransactionPostRenderCache implements TrustedCallbackInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EtTransactionPostRenderCache object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @inheritDoc
   */
  public static function trustedCallbacks() {
    return ['renderViewForm'];
  }

  /**
   * Callback for #post_render_cache; replaces placeholder with et_transaction view form.
   *
   * @param int $id
   *   The et_transaction ID.
   * @param string $view_mode
   *   The view mode the et_transaction should be rendered with.
   * @param string $langcode
   *   The langcode in which the et_transaction should be rendered.
   *
   * @return array
   *   A renderable array containing the et_transaction form.
   */
  public function renderViewForm($id, $view_mode, $langcode = NULL) {
    /** @var \Drupal\expense_tracker\EtTransactionInterface $et_transaction */
    $et_transaction = $this->entityTypeManager->getStorage('et_transaction')->load($id);

    if ($et_transaction) {
      if ($langcode && $et_transaction->hasTranslation($langcode)) {
        $et_transaction = $et_transaction->getTranslation($langcode);
      }
      /** @var \Drupal\et_transaction\Form\EtTransactionViewForm $form_object */
      $form_object = \Drupal::service('class_resolver')->getInstanceFromDefinition('Drupal\expense_tracker\Form\EtTransactionViewForm');
      $form_object->setEtTransaction($et_transaction);
      return \Drupal::formBuilder()->getForm($form_object, \Drupal::request(), $view_mode);
    }
    else {
      return ['#markup' => ''];
    }
  }

}
