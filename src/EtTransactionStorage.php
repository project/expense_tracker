<?php

namespace Drupal\expense_tracker;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;

/**
 * Controller class for et_transactions.
 *
 * This extends the default content entity storage class,
 * adding required special handling for et_transaction entities.
 */
class EtTransactionStorage extends SqlContentEntityStorage implements EtTransactionStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getEtTransactionDuplicates(EtTransactionInterface $et_transaction) {
    $query = \Drupal::entityQuery('et_transaction')->accessCheck(TRUE);
    $query->condition('title', $et_transaction->label());

    if ($et_transaction->id()) {
      $query->condition('id', $et_transaction->id(), '<>');
    }
    return $this->loadMultiple($query->execute());
  }

  /**
   * {@inheritdoc}
   */
  public function getMostRecentEtTransaction() {
    $query = \Drupal::entityQuery('et_transaction')
    ->accessCheck(TRUE)
    ->condition('status', POLL_PUBLISHED)
    ->sort('created', 'DESC')
    ->pager(1);
    return $this->loadMultiple($query->execute());
  }

  /**
   * {@inheritdoc}
   */
  public function getExpiredEtTransactions() {
    $query = $this->database->query('SELECT id FROM {et_transaction_field_data} WHERE (:timestamp > (created + runtime)) AND status = 1 AND runtime <> 0', [':timestamp' => \Drupal::time()->getCurrentTime()]);
    return $this->loadMultiple($query->fetchCol());
  }
}
