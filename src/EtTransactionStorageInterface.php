<?php

namespace Drupal\expense_tracker;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines a common interface for et_transaction entity controller classes.
 */
interface EtTransactionStorageInterface extends EntityStorageInterface {

  /**
   * Get the most recent et_transaction posted on the site.
   *
   * @return mixed
   */
  public function getMostRecentEtTransaction();

  /**
   * Find all duplicates of a et_transaction by matching the title.
   *
   * @param EtTransactionInterface $et_transaction
   *
   * @return mixed
   */
  public function getEtTransactionDuplicates(EtTransactionInterface $et_transaction);

  /**
   * Returns all expired et_transactions.
   *
   * @return \Drupal\expense_tracker\EtTransactionInterface[]
   *
   */
  public function getExpiredEtTransactions();

}
