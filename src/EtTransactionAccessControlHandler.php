<?php

namespace Drupal\expense_tracker;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an access control handler for the et_transaction entity.
 *
 * @see \Drupal\expense_tracker\Entity\EtTransaction
 */
class EtTransactionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {

    $keys = \Drupal::routeMatch()->getParameters()->keys();
    if(isset($keys[0])) {
      $entityTypeIndicator = $keys[0];
      $entity = \Drupal::routeMatch()->getParameter($entityTypeIndicator);
    }
    $route_name = \Drupal::routeMatch()->getRouteName();
    $is_author = false;

    if(isset($entity) && is_object($entity)) {
      if(!$account->isAnonymous() && $account->id() == $entity->get('uid')->target_id) {
        $is_author = true;
      }
    }

    $valid = false;

    switch ($route_name) {
      case 'entity.et_transaction.edit_form':

      if(!$account->isAnonymous() && $account->hasPermission('edit all expense_tracker')) {
        return AccessResult::allowed()->cachePerPermissions();
      } elseif ($account->hasPermission('edit expense_tracker')) {
        if($is_author) {
          $valid = true;
        }
      }

      break;
      case 'entity.et_transaction.delete_form':
      if(!$account->isAnonymous() && $account->hasPermission('delete all expense_tracker')) {
        return AccessResult::allowed()->cachePerPermissions();
      } elseif ($account->hasPermission('delete expense_tracker')) {
        if($is_author) {
          $valid = true;
        }
      }else

      break;

      default:
      return AccessResult::allowed()->cachePerPermissions();
      break;
    }

    if($valid) {
      return AccessResult::allowed()->cachePerPermissions();
    } else {
      throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    $route_name = \Drupal::routeMatch()->getRouteName();

    switch ($route_name) {
      case 'entity.et_transaction.edit_form':
      if($account->hasPermission('edit all expense_tracker')) {

        return AccessResult::allowed()->cachePerPermissions();
      } else {
        if($is_author) {
          return AccessResult::allowedIfHasPermission($account, 'edit expense_tracker');
        }
      }
      break;

      default:
        // code...
      break;
    }


    $is_author = false;
    if(!$account->isAnonymous() && $account->id() == $entity->get('uid')->target_id) {
      $is_author = true;
    }

    $user_roles = $account->getRoles();
    if (!in_array('administrator', $user_roles)) {

      // var_export($operation);
      if ($operation == 'view') {

        if($account->hasPermission('access all expense_tracker')) {
          return AccessResult::allowed()->cachePerPermissions();
        } else {
          if($is_author) {
            return AccessResult::allowedIfHasPermission($account, 'access expense_tracker');
          }
        }
      } elseif ($operation == 'update') {
        if($account->hasPermission('edit all expense_tracker')) {
          return AccessResult::allowed()->cachePerPermissions();
        } else {
          if($is_author) {
            return AccessResult::allowedIfHasPermission($account, 'edit expense_tracker');
          }
        }
      } elseif ($operation == 'delete') {
        if($account->hasPermission('delete all expense_tracker')) {
          return AccessResult::allowed()->cachePerPermissions();
        } else {
          if($is_author) {
            return AccessResult::allowedIfHasPermission($account, 'delete expense_tracker');
          }
        }
      } else {

        if($account->isAnonymous()) {
          return AccessResult::allowedIfHasPermissions($account, [
            'create expense_tracker',
            'access expense_tracker',
            'edit expense_tracker',
            'delete expense_tracker',
            'access all expense_tracker',
            'edit all expense_tracker',
            'delete all expense_tracker',
          ], 'OR');
        }
      }
    }

    return parent::checkAccess($entity, $operation, $account);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {

    $restricted_fields = [
      'uid',
    ];
    if ($operation === 'edit' && in_array($field_definition->getName(), $restricted_fields, TRUE)) {
      return AccessResult::allowedIfHasPermission($account, 'administer expense_tracker');
    }
    return parent::checkFieldAccess($operation, $field_definition, $account, $items);
  }
}
