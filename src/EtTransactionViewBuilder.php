<?php

namespace Drupal\expense_tracker;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Render controller for et_transactions.
 */
class EtTransactionViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL) {
    $entity = $this->entityRepository->getTranslationFromContext($entity, $langcode);

    // Ajax request might send the view mode as a GET argument, use that
    // instead.
    if (\Drupal::request()->query->has('view_mode')) {
      $view_mode = \Drupal::request()->query->get('view_mode');
    }

    $output = parent::view($entity, $view_mode, $langcode);
    $output['#theme_wrappers'] = array('container');
    $output['#attributes']['class'][] = 'et_transaction-view';
    $output['#attributes']['class'][] = $view_mode;

    $output['#et_transaction'] = $entity;
    $output['et_transaction'] = array(
      '#lazy_builder' => [
        'expense_tracker.post_render_cache:renderViewForm',
        // '\Drupal\expense_tracker\EtTransactionPostRenderCache::renderViewForm',
        // '\Drupal\expense_tracker\EtTransactionPostRenderCache',
        [
          'id' => $entity->id(),
          'view_mode' => $view_mode,
          'langcode' => $entity->language()->getId(),
        ],
      ],
      '#create_placeholder' => TRUE,
      '#cache' => [
        'tags' => $entity->getCacheTags(),
      ],
    );

    return $output;

  }

}
