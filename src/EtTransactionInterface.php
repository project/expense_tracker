<?php

namespace Drupal\expense_tracker;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining an et_transaction entity.
 */
interface EtTransactionInterface extends ContentEntityInterface {

  /**
   * Sets the title for the et_transaction.
   *
   * @param string $title
   *   The short title of the feed.
   *
   * @return \Drupal\expense_tracker\EtTransactionInterface
   *   The class instance that this method is called on.
   */
  public function setTitle($title);

  /**
   * Return when the feed was modified last time.
   *
   * @return int
   *   The timestamp of the last time the feed was modified.
   */
  public function getCreated();

  /**
   * Sets the last modification of the feed.
   *
   * @param int $created
   *   The timestamp when the feed was modified.
   *
   * @return \Drupal\expense_tracker\EtTransactionInterface
   *   The class instance that this method is called on.
   */
  public function setCreated($created);


  /**
   * Returns the runtime of the feed in seconds.
   *
   * @return int
   *   The refresh rate of the feed in seconds.
   */
  public function getRuntime();

  /**
   * Sets the runtime of the feed in seconds.
   *
   * @param int $runtime
   *   The refresh rate of the feed in seconds.
   *
   * @return \Drupal\expense_tracker\EtTransactionInterface
   *   The class instance that this method is called on.
   */
  public function setRuntime($runtime);

  /**
   * Returns if the et_transaction is open.
   *
   * @return bool
   *   TRUE if the et_transaction is open.
   */
  public function isOpen();

  /**
   * Returns if the et_transaction is closed.
   *
   * @return bool
   *   TRUE if the et_transaction is closed.
   */
  public function isClosed();

  /**
   * Sets the et_transaction to closed.
   */
  public function close();

  /**
   * Sets the et_transaction to open.
   */
  public function open();

  /**
   * Get all options for this et_transaction.
   *
   * @return array
   *   Associative array of option keys and values.
   */
  public function getOptions();

  /**
   * Get the values of each transaction option for this et_transaction.
   *
   * @return array
   *   Associative array of option values.
   */
  public function getOptionValues();

}
