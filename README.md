Expense tracke (from core) module for Drupal 8 and 9
====================================

This module helps you easily see all your expenses and income in one place. Keep track of all your transactions and reports will help you understand your spending habits.
